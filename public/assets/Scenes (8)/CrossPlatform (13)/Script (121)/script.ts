class ScriptBehavior3 extends Sup.Behavior {
  index: number;
  direction: string;
  target: number;
  
  puzzleActor: Sup.Actor;
  puzzleStart: number;
  
  rightSideComputer: Sup.Actor;
  leftSideComputer: Sup.Actor;
  middleComputer: Sup.Actor;
  startTime: number;
  text2: Sup.Actor;
  
  position: Sup.Math.Vector3;  
  activated = false;
  done = false;
  
  awake() {
    // this.puzzleActor = Sup.getActor(`Puzzle ${this.index}`)
    // this.puzzleStart = this.puzzleActor.getPosition()[this.direction];
    // this.position = this.actor.getPosition();
    // this.position.y -= this.actor.getLocalScale().y / 2;
    
    
    this.rightSideComputer = Sup.getActor(`Linux`)
    this.leftSideComputer = Sup.getActor(`Windows`)
    this.middleComputer = Sup.getActor('Mac')
    this.text2 = Sup.getActor('Text2')
    this.startTime = (new Date()).valueOf();
  }
  
  update() {
    if(!this.done) {
          var eulerRight = this.rightSideComputer.getLocalEulerAngles();
      var eulerLeft = this.leftSideComputer.getLocalEulerAngles();
      if((eulerRight.x > 1 || eulerRight.x < -1.4) && (eulerLeft.x > 1 || eulerLeft.x < -1.4) && this.middleComputer.getLocalEulerAngles().x < 1) {
        this.activate();
      }
    }
    // var euler1 = this.computer1.getLocalEulerAngles();
    // var euler2 = this.computer2.getLocalEulerAngles();
    // if(euler1.z < 0 && euler2.x > 1) {
    //   this.activate();
    // }
//     if (Game.playerBehavior.position.distanceTo(this.position) < 4) {
//       this.actor.modelRenderer.setColor(1.5, 1.5, 1.5);
//       this.actor.getChild("Lever").modelRenderer.setColor(1.5, 1.5, 1.5);
      
//       if (Sup.Input.wasKeyJustReleased("X") || Sup.Input.wasKeyJustReleased("RETURN")) {
//         this.activated = !this.activated;
//         this.done = false;
        
//         let lever = this.actor.getChild("Lever");
//         let angles = new Sup.Math.Vector3(0, 0, 0);
        
//         let init = { angle: Math.PI / 4 };
//         let target = { angle: -Math.PI / 4 };
//         if (!this.activated) {
//           init.angle *= -1;
//           target.angle *= -1;
//         }
        
//         new Sup.Tween(this.actor, init)
//           .to(target, 300)
//           .onUpdate((state) => {
//             angles.z = state.angle;
//             lever.setLocalEulerAngles(angles);
//           })
//           .start();
        
//         this.activate();
//       }
//     } else {
//       this.actor.modelRenderer.setColor(1, 1, 1);
//       this.actor.getChild("Lever").modelRenderer.setColor(1, 1, 1);
//     }
  }

  activate() {
    let position = this.leftSideComputer.getPosition();
    
            this.done = true;

        [
          { x: -10, y: 22, z: 0 },
          { x: -3, y: 25, z: 1 },
          { x: 1, y: 21, z: -1 },
          { x: 5, y: 24, z: 2 },
          { x: 12, y: 19, z: -3 }
        ].forEach((position) => {
          let boxActor = Sup.appendScene("Scenes/CrossPlatform/Box Prefab")[0];
          boxActor.cannonBody.body.position.set(position.x, position.y, position.z);
        })

        Sup.Audio.playSound("Scenes/CrossPlatform/Yay");
    
        new Sup.TextRenderer(this.text2, "Well done! Your time: " + ((new Date().valueOf()) - this.startTime) + " milliseconds", "Scenes/All/Font")

    
//     new Sup.Tween(this.puzzleActor, { position: position[this.direction] })
//       .to({ position: (this.activated) ? this.target : this.puzzleStart }, 2000)
//       .easing(TWEEN.Easing.Cubic.Out)
//       .onUpdate((state) => {
//         // position[this.direction] = state.position;
//         // this.puzzleActor.setPosition(position);
//       })
//       .onComplete(() => {
//         this.done = true;

//         [
//           { x: -10, y: 22, z: 0 },
//           { x: -3, y: 25, z: 1 },
//           { x: 1, y: 21, z: -1 },
//           { x: 5, y: 24, z: 2 },
//           { x: 12, y: 19, z: -3 }
//         ].forEach((position) => {
//           let boxActor = Sup.appendScene("Scenes/CrossPlatform/Box Prefab")[0];
//           boxActor.cannonBody.body.position.set(position.x, position.y, position.z);
//         })

//         Sup.Audio.playSound("Scenes/CrossPlatform/Yay");
//       })
//       .start();
  }
}
Sup.registerBehavior(ScriptBehavior3);
